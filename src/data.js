export const COURSES = [{
        id: "0",
        title: "Bootstrap 4 Tutorial",
        url: "https://www.tutorialspoint.com/bootstrap4/index.htm"
    },
    {
        id: "1",
        title: "CSS Tutorial",
        url: "https://www.tutorialspoint.com/css/index.htm"
    },
    {
        id: "2",
        title: "ES6 Tutorial",
        url: "https://www.tutorialspoint.com/es6/index.htm"
    },
    {
        id: "3",
        title: "Flexbox Tutorial",
        url: "https://www.tutorialspoint.com/flexbox/index.htm"
    },
    {
        id: "4",
        title: "HTML5 Tutorial",
        url: "https://www.tutorialspoint.com/html5/index.htm"
    },
    {
        id: "5",
        title: "Javascript Tutorial",
        url: "https://www.tutorialspoint.com/javascript/index.htm"
    },
];