import React from "react";
import styled from "styled-components";

const MyLink = styled.a`
  text-decoration: none;
`;

const Article = ({ article }) => {
    return (
        <MyLink href={article.url}>{article.title}</MyLink>
    );
}


export default Article;