import React from "react";
import styled from "styled-components";
import Article from "./Article";

const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
`;

const ListItem = styled.li`
  padding: 10px 0;
  border-bottom: 1px solid grey;
`;

const Courses = ({ courses }) => (
  <List>
    {courses.map(article => (
      <ListItem key={article.id}>
        <Article article={article} />
      </ListItem>
    ))}
  </List>
);

export default Courses;